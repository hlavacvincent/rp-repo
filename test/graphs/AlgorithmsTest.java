package graphs;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import static graphs.GraphAlgorithms.*;

import org.junit.Test;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class AlgorithmsTest {

    private Graph readGraph(String file) {
        GraphReader reader = new AdjacencyListGraphReader();
        return reader.readGraph(file);
    }

    @Test
    public void componentsTest() {
        Graph g1 = readGraph("test/graphs/graph1.txt");
        Graph g2 = readGraph("test/graphs/graph2.txt");

        List<Component> comps1 = new ArrayList<>();
        int componentsCount = getComponentCount(g1, comps1);
        assertEquals(componentsCount, 1);
        Component c1 = new Component();
        for (int i = 0; i < 6; ++i) {
            c1.joinComponents(new Component(i));
        }
        assertEquals(comps1.get(0), c1);

        List<Component> comps2 = new ArrayList<>();
        componentsCount = getComponentCount(g2, comps2);
        assertEquals(componentsCount, 2);
        Component c2 = new Component();
        for (int i = 0; i < 3; ++i) {
            c2.joinComponents(new Component(i));
        }
        Component c3 = new Component();
        for (int i = 3; i < 6; ++i) {
            c3.joinComponents(new Component(i));
        }
        assertEquals(new HashSet<>(comps2), new HashSet<>() {{add(c2); add(c3);}});
    }

    @Test
    public void bipartiteTest() {
        Graph g1 = readGraph("test/graphs/graph1.txt");
        Graph g2 = readGraph("test/graphs/graph2.txt");

        Set<Integer> left = new HashSet<>();
        Set<Integer> right = new HashSet<>();
        assertTrue(isBipartite(g1, left, right));
        Set<Integer> l = new HashSet<>() {{add(0); add(1); add(2);}};
        Set<Integer> r = new HashSet<>() {{add(3); add(4); add(5);}};
        assertEquals(left, l);
        assertEquals(right, r);

        assertFalse(isBipartite(g2, null, null));
    }
}
