package graphs;

import java.util.HashSet;

import org.junit.Test;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class ReadTest {

    @Test
    public void readTest() {
        GraphReader reader = new AdjacencyListGraphReader(new Cubic4KPlus2Control());
        Graph g = reader.readGraph("test/graphs/test.txt");

        assertNotNull(g);
        assertEquals(g.getVertexCount(), 6);
        assertEquals(g.getEdgeCount(), 6 * 3 / 2);

        HashSet<Integer> set0 = new HashSet<>() {{add(1); add(2); add(4); }};
        assertEquals(new HashSet<>(g.adjacencyVertices(0)), set0);

        HashSet<Integer> set1 = new HashSet<>() {{add(0); add(2); add(5); }};
        assertEquals(new HashSet<>(g.adjacencyVertices(1)), set1);

        HashSet<Integer> set2 = new HashSet<>() {{add(0); add(1); add(3); }};
        assertEquals(new HashSet<>(g.adjacencyVertices(2)), set2);

        HashSet<Integer> set3 = new HashSet<>() {{add(2); add(4); add(5); }};
        assertEquals(new HashSet<>(g.adjacencyVertices(3)), set3);

        HashSet<Integer> set4 = new HashSet<>() {{add(0); add(3); add(5); }};
        assertEquals(new HashSet<>(g.adjacencyVertices(4)), set4);

        HashSet<Integer> set5 = new HashSet<>() {{add(1); add(3); add(4); }};
        assertEquals(new HashSet<>(g.adjacencyVertices(5)), set5);
    }
}
