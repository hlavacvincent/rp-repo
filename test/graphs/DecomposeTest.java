package graphs;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.Test;
import static org.junit.Assert.assertEquals;

public class DecomposeTest {

    @Test
    public void decomposeTest() {
        GraphReader reader = new AdjacencyListGraphReader(new Cubic4KPlus2Control());
        Graph g = reader.readGraph("test/graphs/test.txt");
        StableTreeDecomposition std = new StableTreeDecomposition(g);

        Set<List<Integer>> decompositions = new HashSet<>();
        decompositions.add(new ArrayList<>() {{add(0); add(3);}});
        decompositions.add(new ArrayList<>() {{add(0); add(5);}});
        decompositions.add(new ArrayList<>() {{add(1); add(3);}});
        decompositions.add(new ArrayList<>() {{add(1); add(4);}});
        decompositions.add(new ArrayList<>() {{add(2); add(4);}});
        decompositions.add(new ArrayList<>() {{add(2); add(5);}});

        assertEquals(decompositions, new HashSet<>(std.getDecomposition()));
    }
}
