package graphs;

import java.util.ArrayList;
import java.util.HashSet;

import org.junit.Test;
import static org.junit.Assert.assertEquals;

public class ReconfigurationTest {

    @Test
    public void reconfigurationTest() {
        GraphReader reader = new AdjacencyListGraphReader(new Cubic4KPlus2Control());
        Graph g = reader.readGraph("test/graphs/test.txt");
        StableTreeDecomposition std = new StableTreeDecomposition(g);
        ReconfGraph rg = std.getReconfigurationGraph();

        assertEquals(rg.getVertexCount(), 6);
        Vertex v1 = new StableTreeDecompVertex(new ArrayList<>() {{add(0); add(3);}});
        Vertex v2 = new StableTreeDecompVertex(new ArrayList<>() {{add(0); add(5);}});
        Vertex v3 = new StableTreeDecompVertex(new ArrayList<>() {{add(1); add(3);}});
        Vertex v4 = new StableTreeDecompVertex(new ArrayList<>() {{add(1); add(4);}});
        Vertex v5 = new StableTreeDecompVertex(new ArrayList<>() {{add(2); add(4);}});
        Vertex v6 = new StableTreeDecompVertex(new ArrayList<>() {{add(2); add(5);}});

        assertEquals(new HashSet<>(rg.adjacencyVertices(rg.getIndex(v1))),
                new HashSet<>() {{add(rg.getIndex(v2)); add(rg.getIndex(v3));}});
        assertEquals(new HashSet<>(rg.adjacencyVertices(rg.getIndex(v2))),
                new HashSet<>() {{add(rg.getIndex(v1)); add(rg.getIndex(v6));}});
        assertEquals(new HashSet<>(rg.adjacencyVertices(rg.getIndex(v3))),
                new HashSet<>() {{add(rg.getIndex(v1)); add(rg.getIndex(v4));}});
        assertEquals(new HashSet<>(rg.adjacencyVertices(rg.getIndex(v4))),
                new HashSet<>() {{add(rg.getIndex(v3)); add(rg.getIndex(v5));}});
        assertEquals(new HashSet<>(rg.adjacencyVertices(rg.getIndex(v5))),
                new HashSet<>() {{add(rg.getIndex(v4)); add(rg.getIndex(v6));}});
        assertEquals(new HashSet<>(rg.adjacencyVertices(rg.getIndex(v6))),
                new HashSet<>() {{add(rg.getIndex(v2)); add(rg.getIndex(v5));}});
    }
}
