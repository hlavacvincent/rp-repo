package graphs;

public interface GraphTypeControl {
    boolean isCorrect(Graph g);
}
