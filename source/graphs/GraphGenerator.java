package graphs;

public interface GraphGenerator {
    void generateGraph(int k);
    void generateGraph(int k, String fileName);
}
