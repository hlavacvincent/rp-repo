package graphs;

import java.io.FileNotFoundException;
import java.io.PrintStream;

public class Circle4KPlus2Generator implements GraphGenerator {
    private final String path;

    public Circle4KPlus2Generator() {
        this("");
    }

    public Circle4KPlus2Generator(String path) {
        this.path = path + "/";
    }

    @Override
    public void generateGraph(int k) {
        generateGraph(k, 4 * k + 2 + "-circle");
    }

    @Override
    public void generateGraph(int k, String fileName) {
        try {
            if (k < 1) throw new IllegalArgumentException("Value of k should be at least 1.");
            try (PrintStream out = new PrintStream(path + fileName)) {
                int vertexCount = 4 * k + 2;
                out.println(vertexCount);
                for (int i = 0; i < vertexCount; ++i) {
                    int value = (i + 1) % vertexCount;
                    if (value > i) out.print(value + " ");
                    value = (i + vertexCount - 1) % vertexCount;
                    if (value > i) out.print(value + " ");
                    value = (i + vertexCount / 2) % vertexCount;
                    if (value > i) out.print(value + " ");
                    out.print("\n");
                }
            }
        } catch (FileNotFoundException | IllegalArgumentException e) {
            System.err.println(e.getMessage());
        }
    }
}
