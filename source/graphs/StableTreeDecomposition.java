package graphs;

import java.util.*;

public class StableTreeDecomposition {
    private final Graph g;
    private final List<VertexColor> vertexColors;
    private final List<Component> vertexComponenets;
    private final List<Component> components;
    private final List<List<Integer>> decomposition;
    private ReconfGraph reconfGraph = null;

    public StableTreeDecomposition(Graph g) {
        this.g = g;
        int vertexCount = g.getVertexCount();
        vertexColors = new ArrayList<>(vertexCount);
        for (int i = 0; i < vertexCount; ++i) {
            vertexColors.add(VertexColor.UNCOLORIZED);
        }
        vertexComponenets = new ArrayList<>(vertexCount);
        for (int i = 0; i < vertexCount; ++i) {
            vertexComponenets.add(null);
        }
        components = new ArrayList<>();
        decomposition = new ArrayList<>();
        decompose();
    }

    private void decompose() {
        // Trivial case of recursion
        if (!vertexColors.contains(VertexColor.UNCOLORIZED)) {
            List<Integer> stableTreeDecomposition = new ArrayList<>();
            for (int i = 0; i < vertexColors.size(); ++i) {
                if (vertexColors.get(i).equals(VertexColor.WHITE)) {
                    stableTreeDecomposition.add(i);
                }
            }
            decomposition.add(stableTreeDecomposition);
            return;
        }

        // Deterministic decisions
        // Coloring with black
        for (int i = 0; i < vertexColors.size(); ++i) {
            if (vertexColors.get(i).equals(VertexColor.WHITE)) {
                for (int vertex : g.adjacencyVertices(i)) {
                    if (vertexColors.get(vertex).equals(VertexColor.UNCOLORIZED)) {
                        colorizeBlack(vertex);
                        return;
                    }
                }
            }
        }
        // Coloring with white
        for (int i = 0; i < vertexColors.size(); ++i) {
            if (vertexColors.get(i).equals(VertexColor.UNCOLORIZED)) {
                for (int vertex1 : g.adjacencyVertices(i)) {
                    if (vertexComponenets.get(vertex1) != null) {
                        for (int vertex2 : g.adjacencyVertices(i)) {
                            if (vertex1 != vertex2 &&
                                    vertexComponenets.get(vertex1).equals(vertexComponenets.get(vertex2))) {
                                colorizeWhite(i);
                                return;
                            }
                        }
                    }
                }
            }
        }

        // Nondeterministic decisions
        int vertex = vertexColors.indexOf(VertexColor.UNCOLORIZED);
        colorizeWhite(vertex);
        colorizeBlack(vertex);
    }

    private void colorizeBlack(int vertex) {
        boolean coloring = true;
        Component newComponent = new Component(vertex);
        Component joiningComponent = new Component();
        List<Component> returnComponents = new ArrayList<>();
        for (Component component : new ArrayList<>(components)) {
            int connectingEdgesCount = newComponent.connectingEdges(component, g);
            if (connectingEdgesCount == 1) {
                joiningComponent.joinComponents(component);
                returnComponents.add(component);
                components.remove(component);
            } else if (connectingEdgesCount > 1) {
                coloring = false;
                break;
            }
        }
        if (coloring) {
            newComponent.joinComponents(joiningComponent);
            components.add(newComponent);
            for (int v : newComponent.getVertices()) {
                vertexComponenets.set(v, newComponent);
            }
            vertexColors.set(vertex, VertexColor.BLACK);
            decompose();
            vertexColors.set(vertex, VertexColor.UNCOLORIZED);
            for (Component c : returnComponents) {
                for (int v : c.getVertices()) {
                    vertexComponenets.set(v, c);
                }
            }
            vertexComponenets.set(vertex, null);
            components.remove(newComponent);
        }
        components.addAll(returnComponents);
    }

    private void colorizeWhite(int vertex) {
        boolean coloring = true;
        vertexColors.set(vertex, VertexColor.WHITE);
        if (components.size() > 1 || vertexColors.contains(VertexColor.UNCOLORIZED)) {
            for (Component component : components) {
                coloring = false;
                for (int cVertex : component.getVertices()) {
                    for (int v : g.adjacencyVertices(cVertex)) {
                        if (vertexColors.get(v).equals(VertexColor.UNCOLORIZED)) {
                            coloring = true;
                            break;
                        }
                    }
                    if (coloring) break;
                }
                if (!coloring) break;
            }
        }
        if (coloring) decompose();
        vertexColors.set(vertex, VertexColor.UNCOLORIZED);
    }

    private void createReconfGraph() {
        // Prepare structures for reconfiguration graph
        List<List<Integer>> adjList = new ArrayList<>();
        for (int i = 0; i < decomposition.size(); ++i) adjList.add(new ArrayList<>());
        List<Vertex> vertices = new ArrayList<>();

        List<Set<Integer>> processed = new ArrayList<>();
        for (int i = 0; i < decomposition.size(); ++i) processed.add(new HashSet<>());

        for (int i1 = 0; i1 < decomposition.size(); ++i1) {
            List<Integer> std = decomposition.get(i1);
            vertices.add(new StableTreeDecompVertex(std));

            for (Integer vertex : std) {
                if (processed.get(i1).contains(vertex)) continue;

                List<Integer> neighbours = new ArrayList<>(g.adjacencyVertices(vertex));
                int root = neighbours.remove(0);

                // BFS
                // Prepare tree structure
                List<Integer> parents = new ArrayList<>();
                List<Integer> depth = new ArrayList<>();
                // TODO We waste with memory because we include stable set vertices as well
                for (int i = 0; i < g.getVertexCount(); ++i) {
                    parents.add(-1);
                    depth.add(-1);
                }

                // Init BFS
                LinkedList<Integer> queue = new LinkedList<>();
                queue.addLast(root);
                depth.set(root, 0);

                // Search BFS
                while (!queue.isEmpty()) {
                    int v = queue.removeFirst();
                    for (int child : g.adjacencyVertices(v)) {
                        if (parents.get(v) == child || std.contains(child)) continue;
                        parents.set(child, v);
                        depth.set(child, depth.get(v) + 1);
                        queue.addLast(child);
                    }
                }

                int v1 = neighbours.remove(0);
                int v2 = neighbours.remove(0); // neighbours is now empty because g is cubic graph

                // Align depth
                while (!depth.get(v1).equals(depth.get(v2))) {
                    if (depth.get(v1).compareTo(depth.get(v2)) < 0) v2 = parents.get(v2);
                    else v1 = parents.get(v1);
                }

                // Find lowest vertex on path
                while (v1 != v2) {
                    v1 = parents.get(v1);
                    v2 = parents.get(v2);
                }

                Set<Integer> neighbourSTD = new TreeSet<>(std);
                neighbourSTD.remove(vertex);
                neighbourSTD.add(v1);

                // TODO: We can create map for finding index in log(n) instead of n
                int i2 = decomposition.indexOf(new ArrayList<>(neighbourSTD));
                adjList.get(i1).add(i2);
                adjList.get(i2).add(i1);

                processed.get(i1).add(vertex);
                processed.get(i2).add(v1);
            }
        }

        reconfGraph = new AdjListReconfGraph(decomposition.size(), adjList, vertices);
    }

    public List<List<Integer>> getDecomposition() {
        return Collections.unmodifiableList(decomposition);
    }

    public ReconfGraph getReconfigurationGraph() {
        if (reconfGraph == null) createReconfGraph();
        return reconfGraph;
    }
}
