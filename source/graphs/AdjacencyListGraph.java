package graphs;

import java.util.Collections;
import java.util.List;

public class AdjacencyListGraph implements Graph {
    private final int vertexCount;
    private final int edgeCount;
    private final List<List<Integer>> adjacencyList;

    protected AdjacencyListGraph(int vextexCount, List<List<Integer>> adjacencyList) {
        this.vertexCount = vextexCount;
        this.adjacencyList = adjacencyList;
        int edgeCount = 0;
        for (List<Integer> adjacencyVertices : adjacencyList) {
            for (int ignored : adjacencyVertices) ++edgeCount;
        }
        this.edgeCount = edgeCount / 2;
    }

    @Override
    public int getVertexCount() {
        return vertexCount;
    }

    @Override
    public int getEdgeCount() {
        return edgeCount;
    }

    @Override
    public boolean containsEdge(int from, int to) {
        return adjacencyList.get(from).contains(to);
    }

    @Override
    public List<Integer> adjacencyVertices(int vertex) {
        return Collections.unmodifiableList(adjacencyList.get(vertex));
    }
}
