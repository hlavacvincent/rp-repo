package graphs;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.List;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.NoSuchElementException;

public class AdjacencyListGraphReader implements GraphReader {
    private final GraphTypeControl graphTypeControl;

    public AdjacencyListGraphReader() {
        this(new NoneControl());
    }

    public AdjacencyListGraphReader(GraphTypeControl graphTypeControl) {
        this.graphTypeControl = graphTypeControl;
    }

    @Override
    public Graph readGraph() {
        Scanner scanner = new Scanner(System.in);
        return readGraph(scanner);
    }

    @Override
    public Graph readGraph(String file) {
        Graph g;
        try (Scanner scanner = new Scanner(new File(file))) {
            g = readGraph(scanner);
        } catch (FileNotFoundException e) {
            System.err.println(e.getMessage());
            g = null;
        }
        return g;
    }

    private Graph readGraph(Scanner scanner) {
        Graph g;
        try {
            String line = scanner.nextLine();
            int vextexCount = Integer.parseInt(line);
            List<List<Integer>> adjacencyList = new ArrayList<>();
            for (int i = 0; i < vextexCount; ++i) {
                adjacencyList.add(new ArrayList<>());
            }

            int edgeStart = 0;
            while (scanner.hasNextLine()) {
                line = scanner.nextLine();
                try (Scanner lineScanner = new Scanner(line)) {
                    int edgeEnd;
                    while (lineScanner.hasNext()) {
                        edgeEnd = Integer.parseInt(lineScanner.next());
                        if (!adjacencyList.get(edgeStart).contains(edgeEnd)) {
                            adjacencyList.get(edgeStart).add(edgeEnd);
                            adjacencyList.get(edgeEnd).add(edgeStart);
                        }
                    }
                }
                ++edgeStart;
            }
            g = new AdjacencyListGraph(vextexCount, adjacencyList);
        } catch (NoSuchElementException | NumberFormatException | IndexOutOfBoundsException e) {
            g = null;
        }
        if (graphTypeControl.isCorrect(g)) return g;
        else return null;
    }
}
