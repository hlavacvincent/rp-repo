package graphs;

public interface ReconfGraph extends Graph {
    Vertex getVertex(int vertex);
    int getIndex(Vertex vertex);
}
