package graphs;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AdjListReconfGraph extends AdjacencyListGraph implements ReconfGraph {
    private final List<Vertex> vertixces;
    private final Map<Vertex, Integer> indexMap;

    protected AdjListReconfGraph(int vextexCount, List<List<Integer>> adjacencyList, List<Vertex> vertices) {
        super(vextexCount, adjacencyList);
        this.vertixces = vertices;
        indexMap = new HashMap<>();
        for (int i = 0; i < vertices.size(); ++i) {
            indexMap.put(vertices.get(i), i);
        }
    }

    @Override
    public Vertex getVertex(int vertex) {
        return vertixces.get(vertex);
    }

    @Override
    public int getIndex(Vertex vertex) {
        return indexMap.get(vertex);
    }
}
