package graphs;

import java.io.FileNotFoundException;
import java.io.PrintStream;

public class Prism2KPlus1SidedGenerator implements GraphGenerator {
    private final String path;

    public Prism2KPlus1SidedGenerator() {
        this("");
    }

    public Prism2KPlus1SidedGenerator(String path) {
        this.path = path + "/";
    }

    @Override
    public void generateGraph(int k) {
        generateGraph(k, 4 * k + 2 + "-prism");
    }

    @Override
    public void generateGraph(int k, String fileName) {
        try {
            if (k < 1) throw new IllegalArgumentException("Value of k should be at least 1.");
            try (PrintStream out = new PrintStream(path + fileName)) {
                int vertexCount = 4 * k + 2;
                int sidesCount = vertexCount / 2;
                out.println(vertexCount);
                for (int i = 0; i < vertexCount; ++i) {
                    int value = (i + 1) % sidesCount;
                    if (i >= sidesCount) value += sidesCount;
                    if (value > i) out.print(value + " ");
                    value = (i + sidesCount - 1) % sidesCount;
                    if (i >= sidesCount) value += sidesCount;
                    if (value > i) out.print(value + " ");
                    value = (i + sidesCount) % vertexCount;
                    if (value > i) out.print(value + " ");
                    out.print("\n");
                }
            }
        } catch (FileNotFoundException | IllegalArgumentException e) {
            System.err.println(e.getMessage());
        }
    }
}
