package graphs;

import java.util.*;

public class GraphAlgorithms {

    // Returns number of components, store components in list "components" if it is not null
    public static int getComponentCount(Graph g, List<Component> components) {
        int componentCount = 0;
        boolean save = components != null;
        final int vertexCount = g.getVertexCount();

        List<Boolean> visited = new ArrayList<>();
        for (int i = 0; i < vertexCount; ++i) {
            visited.add(false);
        }

        LinkedList<Integer> queue = new LinkedList<>();
        int nextIndex = 0;
        while (nextIndex < vertexCount) {
            if (save) components.add(new Component());
            visited.set(nextIndex, true);
            queue.addLast(nextIndex);

            while (!queue.isEmpty()) {
                int vertex = queue.removeFirst();
                if (save) components.get(componentCount).joinComponents(new Component(vertex));

                for (int child : g.adjacencyVertices(vertex)) {
                    if (!visited.get(child)) {
                        visited.set(child, true);
                        queue.addLast(child);
                    }
                }
            }
            ++componentCount;

            while (nextIndex < vertexCount) {
                if (!visited.get(nextIndex)) break;
                ++nextIndex;
            }
        }

        return componentCount;
    }

    // Return true if graph is bipartite, store parts in sets "left" and "right" if both are not null
    public static boolean isBipartite(Graph g, Set<Integer> left, Set<Integer> right) {
        boolean save = (left != null) && (right != null);
        final int vertexCount = g.getVertexCount();

        List<Integer> part = new ArrayList<>();
        for (int i = 0; i < vertexCount; ++i) {
            part.add(-1);
        }

        LinkedList<Integer> queue = new LinkedList<>();
        int nextIndex = 0;
        while (nextIndex < vertexCount) {
            part.set(nextIndex, 0);
            queue.addLast(nextIndex);

            while (!queue.isEmpty()) {
                int vertex = queue.removeFirst();
                int vertexPart = part.get(vertex);
                int childPart = vertexPart == 0 ? 1 : 0;
                if (save) {
                    if (vertexPart == 0) left.add(vertex);
                    else if (vertexPart == 1) right.add(vertex);
                }

                for (int child : g.adjacencyVertices(vertex)) {
                    if (part.get(child) == -1) {
                        part.set(child, childPart);
                        queue.addLast(child);
                    } else if (part.get(child) != childPart) {
                        return false;
                    }
                }
            }

            while (nextIndex < vertexCount) {
                if (part.get(nextIndex) == -1) break;
                ++nextIndex;
            }
        }
        return true;
    }
}
