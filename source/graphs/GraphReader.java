package graphs;

public interface GraphReader {
    Graph readGraph();
    Graph readGraph(String file);
}
