package graphs;

import java.util.List;

public class Cubic4KPlus2Control implements GraphTypeControl {

    @Override
    public boolean isCorrect(Graph g) {
        if (g == null) return false;
        if ((g.getVertexCount() - 2) % 4 != 0) return false;
        for (int i = 0; i < g.getVertexCount(); ++i) {
            List<Integer> adjacencyVertices = g.adjacencyVertices(i);
            if (adjacencyVertices.size() != 3) return false;
            if (adjacencyVertices.contains(i)) return false;
        }
        return true;
    }
}
