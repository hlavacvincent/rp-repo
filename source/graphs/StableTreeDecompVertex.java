package graphs;

import java.util.Collections;
import java.util.List;
import java.util.Objects;

public class StableTreeDecompVertex implements Vertex {
    private final List<Integer> stableSet;

    public StableTreeDecompVertex(List<Integer> stableSet) {
        this.stableSet = stableSet;
    }

    public List<Integer> getStableSet() {
        return Collections.unmodifiableList(stableSet);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        StableTreeDecompVertex that = (StableTreeDecompVertex) o;
        return Objects.equals(stableSet, that.stableSet);
    }

    @Override
    public int hashCode() {
        return Objects.hash(stableSet);
    }
}
