package graphs;

import java.util.List;

public interface Graph {
    int getVertexCount();
    int getEdgeCount();
    boolean containsEdge(int from, int to);
    List<Integer> adjacencyVertices(int vertex);
}
