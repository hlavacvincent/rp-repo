package graphs;

import java.util.Collections;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

public class Component {
    private final Set<Integer> vertices;

    public Component() {
        vertices = new HashSet<>();
    }

    public Component(int vertex) {
        this();
        vertices.add(vertex);
    }

    public Set<Integer> getVertices() {
        return Collections.unmodifiableSet(vertices);
    }

    public void joinComponents(Component c) {
        vertices.addAll(c.getVertices());
    }

    public int connectingEdges(Component c, Graph g) {
        int connectingEdges = 0;
        for (int vertex : vertices) {
            for (int secondVertex : g.adjacencyVertices(vertex)) {
                if (c.getVertices().contains(secondVertex)) ++connectingEdges;
            }
        }
        return connectingEdges;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Component component = (Component) o;
        return Objects.equals(vertices, component.getVertices());
    }

    @Override
    public int hashCode() {
        return Objects.hash(vertices);
    }
}
