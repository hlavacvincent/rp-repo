import graphs.*;

import java.io.*;
import java.util.List;
import java.util.Optional;

public class STDGenerationTest {
    public static void main(String[] args) {
        /* First arg: path with name of file which contains graph for decomposition
         */
        String inFile = args[0];
        String outFile;
        if (inFile.contains("/")) {
            outFile = inFile.substring(0, inFile.lastIndexOf('/') + 1) +
                    "out_" + inFile.substring(inFile.lastIndexOf('/') + 1);
        } else outFile = "out_" + inFile;

        GraphReader graphReader = new AdjacencyListGraphReader(new Cubic4KPlus2Control());
        Optional<Graph> graph = Optional.ofNullable(graphReader.readGraph(inFile));
        long startTime = System.currentTimeMillis();
        Optional<StableTreeDecomposition> std = graph.map(StableTreeDecomposition::new);
        long endtime = System.currentTimeMillis();
        System.out.println(endtime - startTime);

        std.ifPresent(stableTreeDecomposition -> {
            try (PrintStream out = new PrintStream(outFile)) {
                for (List<Integer> decomposition : stableTreeDecomposition.getDecomposition()) {
                    for (int i = 0; i < decomposition.size(); ++i) {
                        out.print(decomposition.get(i));
                        if (i != decomposition.size() - 1) out.print("-");
                    }
                    out.print("\n");
                }
            } catch (FileNotFoundException e) {
                System.err.println(e.getMessage());
            }
        });
    }
}
