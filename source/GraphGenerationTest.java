import graphs.Circle4KPlus2Generator;
import graphs.GraphGenerator;
import graphs.Prism2KPlus1SidedGenerator;

import java.util.Optional;

public class GraphGenerationTest {
    public static void main(String[] args) {
        /* First arg: type of 4k+2 vertex cubic graph (circle/prism)
         * Second arg: integer value k
         * Third arg: path to output file, not name, this will be generated
         */
        Optional<GraphGenerator> graphGenerator = switch (args[0]) {
            case "circle" -> Optional.of(new Circle4KPlus2Generator(args[2]));
            case "prism" -> Optional.of(new Prism2KPlus1SidedGenerator(args[2]));
            default -> Optional.empty();
        };
        try {
            graphGenerator.ifPresent(generator -> generator.generateGraph(Integer.parseInt(args[1])));
        } catch (NumberFormatException e) {
            System.err.println(e.getMessage());
        }
    }
}
