import graphs.*;

import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.util.*;

public class RGPropertiesTest {
    public static void main(String[] args) {
        /* First arg: path with name of file which contains graph for decomposition
         */
        String inFile = args[0];
        String outFile;
        if (inFile.contains("/")) {
            outFile = inFile.substring(0, inFile.lastIndexOf('/') + 1) +
                    "out_" + inFile.substring(inFile.lastIndexOf('/') + 1) + "_properties";
        } else outFile = "out_" + inFile + "_properties";

        GraphReader graphReader = new AdjacencyListGraphReader(new Cubic4KPlus2Control());
        Optional<Graph> graph = Optional.ofNullable(graphReader.readGraph(inFile));
        Optional<StableTreeDecomposition> std = graph.map(StableTreeDecomposition::new);
        long startTime = System.currentTimeMillis();
        Optional<ReconfGraph> reconfGraph = std.map(StableTreeDecomposition::getReconfigurationGraph);
        long endtime = System.currentTimeMillis();
        System.out.println(endtime - startTime);

        reconfGraph.ifPresent(reconfigurationGraph -> {
            try (PrintStream out = new PrintStream(outFile)) {
                List<Component> components = new ArrayList<>();
                int componentsCount = GraphAlgorithms.getComponentCount(reconfigurationGraph, components);
                out.format("Components count: %d\n", componentsCount);

                if (componentsCount > 1) {
                    for (Component c : components) {
                        for (Integer vertex : c.getVertices()) {
                            out.format("%d ", vertex);
                        }
                        out.print("\n");
                    }
                }

                out.print("\n");
                Set<Integer> leftPart = new TreeSet<>();
                Set<Integer> rightPart = new TreeSet<>();
                boolean bipartite = GraphAlgorithms.isBipartite(reconfigurationGraph, leftPart, rightPart);
                out.format("Is%s bipartite\n", (bipartite) ? "" : " not");

                if (bipartite) {
                    for (Integer vertex : leftPart) {
                        out.format("%d ", vertex);
                    }
                    out.print("\n");

                    for (Integer vertex : rightPart) {
                        out.format("%d ", vertex);
                    }
                    out.print("\n");
                }
            } catch (FileNotFoundException e) {
                System.err.println(e.getMessage());
            }
        });
    }
}
